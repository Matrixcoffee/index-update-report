### Index difference report
Report generated on Friday, January 4 2019, Week 01

Comparing differences between index-v1-2018-12-28-Fri.jar and index-v1-2019-01-04-Fri.jar

These are the apps that were updated, and are candidates for inclusion in TWIF. As usual, all new apps will automatically get a mention, but a mini-announcement that we can include is highly appreciated. As for the updated apps, there are way too many to include all of them, so please tell us which ones are important, and why.

#### Removed Apps

* **[Calendar](https://f-droid.org/wiki/page/com.simplemobiletools.calendar)**
* **[Contacts](https://f-droid.org/wiki/page/com.simplemobiletools.contacts)**
* **[File Manager](https://f-droid.org/wiki/page/com.simplemobiletools.filemanager)**
* **[Gallery](https://f-droid.org/wiki/page/com.simplemobiletools.gallery)**
* **[Notes](https://f-droid.org/wiki/page/com.simplemobiletools.notes)**
* **[AddressToGPS](https://f-droid.org/wiki/page/me.danielbarnett.addresstogps)**

6 apps were removed

#### Added Apps

* **[DCipher](https://f-droid.org/app/com.adityakamble49.dcipher)**: Secret text with 100% offline encryption.
* **[arXiv eXplorer](https://f-droid.org/app/com.gbeatty.arxiv)**
* **[Vanilla Metadata Fetch](https://f-droid.org/app/com.kanedias.vanilla.metadata)**: Vanilla music player metadata retrieval plugin.
* **[apps_Packages Info](https://f-droid.org/app/com.oF2pks.applicationsinfo)**: Updated applicationsinfos with colors & mini-tags (and basic fixes).
* **[Classical Music Tagger](https://f-droid.org/app/de.kromke.andreas.musictagger)**: A plain audio file metadata editor especially for classical music.
* **[Tri Peaks Solitaire for Android](https://f-droid.org/app/eu.veldsoft.tri.peaks)**: Solitaire card game.
* **[My Wifi Passwords](https://f-droid.org/app/info.aario.mywifipasswords)**: View your wi-fi passwords.
* **[Meritous](https://f-droid.org/app/net.asceai.meritous)**: Dungeon crawl game.
* **[PhoneTrack](https://f-droid.org/app/net.eneiluj.nextcloud.phonetrack)**
* **[35C3 Wifi Setup](https://f-droid.org/app/nl.eventinfra.wifisetup)**: Official noc application for connecting to the 35c3 wifi.
* **[SuperUStats](https://f-droid.org/app/superustats.tool.android)**

11 apps were added

#### Updated Apps

* **[Reader for Selfoss](https://f-droid.org/app/apps.amine.bou.readerforselfoss)** was updated from 1718123471-github to 1719010021-github
* **[Mi Manga Nu](https://f-droid.org/app/ar.rulosoft.mimanganu)** was [updated](https://github.com/raulhaag/MiMangaNu/blob/HEAD/README.md) from 1.93 to 1.94
* **[DAVx⁵](https://f-droid.org/app/at.bitfire.davdroid)** was [updated](https://forums.bitfire.at/category/4/davdroid?tag=announcement) from 2.0.7-ose to 2.1-ose
* **[VPN Hotspot](https://f-droid.org/app/be.mygod.vpnhotspot)** was updated from 2.1.0 to 2.1.1
* **[EteSync](https://f-droid.org/app/com.etesync.syncadapter)** was [updated](https://github.com/etesync/android/blob/HEAD/ChangeLog.md) from 0.22.4 to 0.22.5
* **[Aria2Android](https://f-droid.org/app/com.gianlu.aria2android)** was updated from 1.11.1 to 2.0.2
* **[Audio Recorder](https://f-droid.org/app/com.github.axet.audiorecorder)** was updated from 3.2.50 to 3.2.52
* **[Book Reader](https://f-droid.org/app/com.github.axet.bookreader)** was updated from 1.11.15 to 1.11.23
* **[Syncthing-Fork](https://f-droid.org/app/com.github.catfriend1.syncthingandroid)** was [updated](https://github.com/Catfriend1/syncthing-android/releases) from 0.14.54.4 to 1.0.0.1
* **[Stocks Widget](https://f-droid.org/app/com.github.premnirmal.tickerwidget)** was updated from 2.5.22 to 3.0.3
* **[ABCore](https://f-droid.org/app/com.greenaddress.abcore)** was [updated](https://github.com/greenaddress/abcore/releases) from 0.68 to 0.69
* **[Quasseldroid](https://f-droid.org/app/com.iskrembilen.quasseldroid)** was updated from 0.11.7 to v1.0.11
* **[WaveUp](https://f-droid.org/app/com.jarsilio.android.waveup)** was [updated](https://gitlab.com/juanitobananas/wave-up/blob/HEAD/CHANGELOG.md) from 2.5.3 to 2.5.4
* **[Identify Dog Breeds](https://f-droid.org/app/com.jstappdev.dbclf)** was updated from 32 to 34
* **[Notification Notes](https://f-droid.org/app/com.khuttun.notificationnotes)** was updated from 1.7 to 1.8
* **[A Time Tracker](https://f-droid.org/app/com.markuspage.android.atimetracker)** was [updated](https://github.com/netmackan/ATimeTracker/releases) from 0.23 to 0.50
* **[QKSMS](https://f-droid.org/app/com.moez.QKSMS)** was [updated](https://github.com/moezbhatti/qksms/releases) from 3.5.4 to 3.6.1
* **[Get Off Your Phone](https://f-droid.org/app/com.nephi.getoffyourphone)** was updated from 6.0r to 6.1r
* **[Nextcloud Talk](https://f-droid.org/app/com.nextcloud.talk2)** was [updated](https://github.com/nextcloud/talk-android/releases) from 3.1.2 to 3.1.3
* **[(F-Droid) Kaltura Device Info](https://f-droid.org/app/com.oF2pks.kalturadeviceinfos)** was updated from 1.3.1-5 to 1.3.1-6
* **[NZ Tides](https://f-droid.org/app/com.palliser.nztides)** was updated from 11 to 12
* **[Shattered Pixel Dungeon](https://f-droid.org/app/com.shatteredpixel.shatteredpixeldungeon)** was updated from 0.7.1a to 0.7.1b
* **[Lyrically](https://f-droid.org/app/com.shkmishra.lyrically)** was [updated](https://github.com/shkcodes/Lyrically/releases) from 0.54 to 0.55
* **[Simple Contacts Pro](https://f-droid.org/app/com.simplemobiletools.contacts.pro)** was [updated](https://github.com/SimpleMobileTools/Simple-Contacts/blob/HEAD/CHANGELOG.md) from 6.1.1 to 6.1.2
* **[Simple Gallery Pro](https://f-droid.org/app/com.simplemobiletools.gallery.pro)** was [updated](https://github.com/SimpleMobileTools/Simple-Gallery/blob/HEAD/CHANGELOG.md) from 6.1.1 to 6.1.3
* **[Simple Notes Pro](https://f-droid.org/app/com.simplemobiletools.notes.pro)** was [updated](https://github.com/SimpleMobileTools/Simple-Notes/blob/HEAD/CHANGELOG.md) from 6.1.0 to 6.1.1
* **[Carnet](https://f-droid.org/app/com.spisoft.quicknote)** was [updated](https://qn.phie.ovh/fdroid_changelog) from 0.10 to 0.12
* **[Privacy Browser](https://f-droid.org/app/com.stoutner.privacybrowser.standard)** was [updated](https://www.stoutner.com/privacy-browser/changelog/) from 2.15 to 2.15.1
* **[Open Link With](https://f-droid.org/app/com.tasomaniac.openwith.floss)** was [updated](https://github.com/tasomaniac/OpenLinkWith/releases) from 2.3-floss to 2.5-floss
* **[baresip](https://f-droid.org/app/com.tutpro.baresip)** was updated from 5.2.1 to 5.3.0
* **[FOSS Browser](https://f-droid.org/app/de.baumann.browser)** was [updated](https://github.com/scoute-dich/browser/blob/HEAD/CHANGELOG.md) from 6.0 to 6.1.1
* **[NotificationLog](https://f-droid.org/app/de.jl.notificationlog)** was updated from 1.1 to 1.2
* **[Unpopular Music Player](https://f-droid.org/app/de.kromke.andreas.unpopmusicplayerfree)** was [updated](https://gitlab.com/AndreasK/unpopular-music-player/blob/HEAD/app/src/main/assets/version-history.txt) from 1.44.3 to 1.50
* **[Senior Launcher](https://f-droid.org/app/de.nodomain.tobihille.seniorlauncher)** was updated from 1.5 to 1.6
* **[Pix-Art Messenger](https://f-droid.org/app/de.pixart.messenger)** was [updated](https://github.com/kriztan/Pix-Art-Messenger/blob/HEAD/CHANGELOG.md) from 2.1.4 to 2.1.5
* **[Offi Directions](https://f-droid.org/app/de.schildbach.oeffi)** was [updated](https://gitlab.com/oeffi/oeffi/raw/HEAD/oeffi/CHANGES) from 10.4.5-aosp to 10.4.7-aosp
* **[DarkCroc Theme](https://f-droid.org/app/de.spiritcroc.darkcroc.substratum)** was [updated](https://github.com/SpiritCroc/DarkCroc-Android-theme/releases) from 1.1 to 1.2
* **[EnigmAndroid](https://f-droid.org/app/de.vanitasvitae.enigmandroid)** was [updated](https://github.com/vanitasvitae/EnigmAndroid/blob/HEAD/CHANGELOG.txt) from 1.0.1-12.01.2018 to 1.0.2-01.01.2019
* **[Yaacc](https://f-droid.org/app/de.yaacc)** was [updated](https://github.com/tobexyz/yaacc-code/releases) from 2.0.2 to 2.1.0
* **[FairEmail](https://f-droid.org/app/eu.faircode.email)** was [updated](https://github.com/M66B/open-source-email/releases) from 1.240 to 1.253
* **[Fruit Radar](https://f-droid.org/app/eu.quelltext.mundraub)** was updated from 1.171 to 1.172
* **[Altcoin Prices](https://f-droid.org/app/eu.uwot.fabio.altcoinprices)** was [updated](https://gitlab.com/cfabio/AltcoinPrices/tags) from 1.6.18 to 1.6.19
* **[AnLinux](https://f-droid.org/app/exa.lnx.a)** was updated from 4.94 to 5.00
* **[Vädret](https://f-droid.org/app/fi.kroon.vadret)** was [updated](https://github.com/vadret/android/blob/HEAD/app/src/main/res/raw/changelog.md) from 0.1.5 to 0.1.6
* **[Mastalab](https://f-droid.org/app/fr.gouv.etalab.mastodon)** was [updated](https://gitlab.com/tom79/mastalab/tags) from 1.50.3 to 1.62.1
* **[KISS launcher](https://f-droid.org/app/fr.neamar.kiss)** was [updated](https://github.com/Neamar/KISS/releases) from 3.6.1 to 3.7.0
* **[OOS Firmware Extractor](https://f-droid.org/app/fr.witchdoctors.c4ffein.oosfirmwareextractor)** was updated from 0.2.0 to 0.2.1
* **[Riot.im](https://f-droid.org/app/im.vector.alpha)** was [updated](https://github.com/vector-im/riot-android/blob/HEAD/CHANGES.rst) from 0.8.20 to 0.8.21
* **[35C3 Schedule](https://f-droid.org/app/info.metadude.android.congress.schedule)** was updated from 1.38.1 to 1.38.4
* **[PSLab](https://f-droid.org/app/io.pslab)** was updated from 2.0.8 to 2.0.9
* **[MaterialFBook](https://f-droid.org/app/me.zeeroooo.materialfb)** was [updated](https://github.com/ZeeRooo/MaterialFBook/blob/HEAD/README.md#changelog) from 3.8.6 to 3.8.7
* **[Markor](https://f-droid.org/app/net.gsantner.markor)** was [updated](https://github.com/gsantner/markor/blob/HEAD/CHANGELOG.md) from 1.5.1 to 1.6.0
* **[Minetest](https://f-droid.org/app/net.minetest.minetest)** was updated from 0.4.16.17 to 0.4.17.21
* **[PeerTube](https://f-droid.org/app/net.schueller.peertube)** was updated from 1.0.13 to 1.0.20
* **[Syncthing Lite](https://f-droid.org/app/net.syncthing.lite)** was [updated](https://github.com/syncthing/syncthing-lite/releases) from 0.3.10 to 0.3.11
* **[SleepyWifi](https://f-droid.org/app/nl.devluuk.sleepywifi)** was [updated](https://github.com/DevLuuk/SleepyWifi/releases) from 1.1 to 1.3
* **[Disroot App](https://f-droid.org/app/org.disroot.disrootapp)** was updated from 1.1.0 to 1.1.1
* **[Symphony](https://f-droid.org/app/org.fitchfamily.android.symphony)** was [updated](https://github.com/n76/Symphony/blob/HEAD/CHANGELOG.md) from 1.1.9 to 1.1.10
* **[G-Droid](https://f-droid.org/app/org.gdroid.gdroid)** was [updated](https://gitlab.com/gdroid/gdroidclient/tags) from 0.5.0 to 0.5.2
* **[DroidFish](https://f-droid.org/app/org.petero.droidfish)** was updated from 1.67 to 1.72
* **[Your local weather](https://f-droid.org/app/org.thosp.yourlocalweather)** was [updated](https://raw.githubusercontent.com/thuryn/your-local-weather/HEAD/CHANGELOG) from 4.5.3 to 4.6.3
* **[Kore](https://f-droid.org/app/org.xbmc.kore)** was [updated](https://github.com/xbmc/Kore/blob/HEAD/CHANGELOG.md) from v2.4.4 to v2.4.5
* **[Kwik DMAP](https://f-droid.org/app/player.efis.mfd)** was [updated](https://github.com/ninelima/kwikEFIS/blob/HEAD/CHANGELOG.md) from 2.1 to 2.2
* **[Kwik EFIS](https://f-droid.org/app/player.efis.pfd)** was [updated](https://github.com/ninelima/kwikEFIS/blob/HEAD/CHANGELOG.md) from 4.1 to 4.2
* **[Emerald Launcher](https://f-droid.org/app/ru.henridellal.emerald)** was [updated](https://github.com/HenriDellal/emerald/releases) from 0.6.0.3 to 0.6.1
* **[FiSSH](https://f-droid.org/app/science.iodev.fissh)** was updated from 2.7 to 3.0
* **[Manyverse](https://f-droid.org/app/se.manyver)** was [updated](https://gitlab.com/staltz/manyverse/blob/HEAD/CHANGELOG.md) from 0.1811.14-beta to 0.1901.2-beta
* **[Drum On!](https://f-droid.org/app/se.tube42.drum.android)** was updated from 0.2.8 to 0.2.9
* **[UserLAnd](https://f-droid.org/app/tech.ula)** was [updated](https://github.com/CypherpunkArmory/UserLAnd/releases) from 1.0.3 to 2.0.0
* **[Monochromatic](https://f-droid.org/app/uk.co.richyhbm.monochromatic)** was updated from 0.4.1 to 0.5.0
* **[RoMote](https://f-droid.org/app/wseemann.media.romote)** was updated from 1.0.10 to 1.0.11
* **[Firefly III Mobile](https://f-droid.org/app/xyz.hisname.fireflyiii)** was updated from 1.1.0 to 1.1.2

72 apps were upgraded

#### Beta Updates

* **[Nextcloud Talk](https://f-droid.org/app/com.nextcloud.talk2)** was [updated](https://github.com/nextcloud/talk-android/releases) from 3.1.2 to 3.2.0beta1

1 apps had beta upgrades

